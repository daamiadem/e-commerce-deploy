package com.qa.project.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EnrichedSubCategoryDto {
    private SubCategoryDto subCategoryDto;
    private CategoryDto categoryDto;
}
