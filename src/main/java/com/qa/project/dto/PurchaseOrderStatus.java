package com.qa.project.dto;

public enum PurchaseOrderStatus {
    OPENED, CONFIRMED
}
