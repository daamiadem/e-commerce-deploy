package com.qa.project.dto;

public enum ProductStatus {
    INSTOCK, LOWSTOCK, OUTOFSTOCK, DELETED
}
