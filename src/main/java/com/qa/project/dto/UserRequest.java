package com.qa.project.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
@AllArgsConstructor
@NoArgsConstructor
@Data

public class UserRequest {

    private String firstname;
    private String lastname;
    private String email;
    private String password;
    private Date dateNaissance;
    private String role;
    private String phoneNumber;


}
