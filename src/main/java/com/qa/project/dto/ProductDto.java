package com.qa.project.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductDto {
    private Long id;
    private String name;
    private String description;
    private String image;
    private SubCategoryDto subCategoryDto;
    private Long price;
    private Long quantity;
    private ProductStatus productStatus;
    private  ClientDto sellerDto;
}
