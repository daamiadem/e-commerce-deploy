package com.qa.project.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderItemDto {
    private Long id;
    private int quantity;
    private BigDecimal unitPrice;
//    private PurchaseOrderDto purchaseOrderDto;
    private ProductDto productDto;
    private OrderStatus orderStatus;
}
