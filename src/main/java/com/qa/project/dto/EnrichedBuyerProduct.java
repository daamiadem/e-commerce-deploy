package com.qa.project.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EnrichedBuyerProduct {
    private ProductDto productDto;
    private List<ReviewDto> reviewDtos;
    private CategoryDto categoryDto;
}
