package com.qa.project.dto;

public enum OrderStatus {
    PENDING, CONFIRMED, DELIVERED, CANCELLED
}
