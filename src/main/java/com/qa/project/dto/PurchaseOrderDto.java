package com.qa.project.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PurchaseOrderDto {
    private Long id;
    private LocalDateTime orderDate;
    private BigDecimal totalAmount;
    private ClientDto userDto;
    private AddressDto shippingAddressDto;
    private List <OrderItemDto> orderItemDtos;
    //private OrderStatus orderStatus;
    private PurchaseOrderStatus purchaseOrderStatus;
}
