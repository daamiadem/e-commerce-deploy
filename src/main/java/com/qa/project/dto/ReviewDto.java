package com.qa.project.dto;

import com.qa.project.model.Client;
import com.qa.project.model.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ReviewDto {
    private Long id;
    private String comment;
    private int rating;
    private ProductDto productDto;
    private ClientDto userDto;
}
