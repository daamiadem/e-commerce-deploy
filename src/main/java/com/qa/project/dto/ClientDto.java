package com.qa.project.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Email;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Validated
public class ClientDto {
    private Long id;
    private String nom;
    @Email
    private String email;
    private String phoneNumber;
    private String prenom;
    private Date dateNaissance;
    private Date dateCreationCompte;
    private String pays;
    private Boolean verified;
}
