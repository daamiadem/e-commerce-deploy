package com.qa.project.model;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Client{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String nom;
    private String prenom;
    @Email
    private String email;
    private String phoneNumber;

    @Column(name = "date_naissance")
    private Date dateNaissance;

    @Column(name = "date_creation_compte")
    private Date dateCreationCompte;
    private String pays;
    private Boolean verified;
    @OneToMany(mappedBy = "client", cascade = CascadeType.ALL)
    private List<Review> reviews;

    @OneToMany(mappedBy = "seller", cascade = CascadeType.ALL)
    private List<Product> products;


}
