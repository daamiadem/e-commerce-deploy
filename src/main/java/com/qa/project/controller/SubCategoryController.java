package com.qa.project.controller;

import com.qa.project.dto.ClientDto;
import com.qa.project.dto.EnrichedSubCategoryDto;
import com.qa.project.dto.PurchaseOrderDto;
import com.qa.project.dto.SubCategoryDto;
import com.qa.project.service.SubCategoryService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/sub-category")
@AllArgsConstructor
@Slf4j
public class SubCategoryController {
    private final SubCategoryService subCategoryService;
    @GetMapping("/find-all")
    ResponseEntity<List<SubCategoryDto>> getAll(){
        log.info("get all subcategory");
        List<SubCategoryDto> allSubCategory = subCategoryService.getAllSubCategory();
        return ResponseEntity.ok().body(allSubCategory);
    }
    @PostMapping(path = "/addSubCategory")
    ResponseEntity<SubCategoryDto> addSubCategory(@RequestParam(name = "categoryId") Long categoryId, @RequestParam(name = "subName") String subName){
        log.info("Add new subcategory");
        SubCategoryDto subCategoryDto1 = subCategoryService.addSubCategory(categoryId,subName);
        return  ResponseEntity.ok().body(subCategoryDto1);
    }

    @DeleteMapping(path = "/deleteSubCategory")
    ResponseEntity<Long> deleteSubCategory(@RequestParam(name = "id_subcategory") Long id_subcategory){
        log.info("Add new subcategory");
        Long deletedSubCategory = subCategoryService.deleteSubCategory(id_subcategory);
        return  ResponseEntity.ok().body(deletedSubCategory);
    }
    @GetMapping(path = "/get-subCategory-by-id")
    ResponseEntity<EnrichedSubCategoryDto> getSubcategoryById(@RequestParam(name = "subId") Long subId){
        log.info("get subCategory by id");
        EnrichedSubCategoryDto subCategoryDto = subCategoryService.getSubcategoryById(subId);
        return  ResponseEntity.ok().body(subCategoryDto);
    }


}
