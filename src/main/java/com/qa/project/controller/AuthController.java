package com.qa.project.controller;


import com.qa.project.dto.*;
import com.qa.project.dto.security.JwtTokenProvider;
import com.qa.project.model.security.Myuser;
import com.qa.project.model.security.Role;
import com.qa.project.service.ClientService;
import com.qa.project.service.EmailService;
import com.qa.project.service.RoleService;
import com.qa.project.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

@RestController
@RequestMapping("/api/auth")
@Slf4j
public class AuthController {


    @Autowired
    private UserService userService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;
    @Autowired
    private RoleService roleService;
    @Autowired
    private EmailService emailService;
    @Autowired
    private ClientService clientService;

    @PostMapping("/signup")
    public ResponseEntity<String> signUp(@RequestBody UserRequest userRequest) {
        Myuser user = userService.signUp(userRequest);
        return ResponseEntity.ok("User signed up successfully");
    }
    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody LoginRequest loginRequest) {
        ClientDto clientDto = clientService.getClientByEmail(loginRequest.getUsername());
        if (Objects.nonNull(clientDto) && clientDto.getVerified().equals(false)){
            HttpHeaders headers = new HttpHeaders();
            headers.add("error-code", "1001");
            return ResponseEntity.badRequest().headers(headers).body("Your account is not yet verified !");
        }

        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword())
            );
        } catch (BadCredentialsException e) {
            return new ResponseEntity<>("Invalid username or password", HttpStatus.UNAUTHORIZED);
        }

        Optional<UserDetails> userDetails = userService.findByUsername(loginRequest.getUsername())
                .map(user -> (UserDetails) user);

        String token = jwtTokenProvider.generateToken(userDetails.orElse(null));
        Optional<Myuser> myuser = userService.findByUsername(userDetails.map(UserDetails::getUsername).orElse(null));
        Role role = roleService.getRoleByUsername(myuser.get().getUsername());
        Map<Object, Object> model = new HashMap<>();
        model.put("username", userDetails.map(UserDetails::getUsername).orElse(null));
        model.put("role", role.getName());
        model.put("token", token);

        return ResponseEntity.ok().body(model);
    }

    @PostMapping("/passwordForget")
    public ResponseEntity<Void> forgetPassword(@RequestParam(name = "email") String email) throws IOException {
        log.info("request for password forgot");
        emailService.sendPasswordResetEmail(email);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/resetPassword")
    public ResponseEntity<Void> resetPassword(@RequestParam(name = "password") String password, @RequestParam(name = "verificationToken") String verificationToken) throws IOException {
        log.info("reset password Now !");
        emailService.resetPassword(password,verificationToken);
        return ResponseEntity.ok().build();
    }









}


