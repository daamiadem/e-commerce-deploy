package com.qa.project.controller;

import com.qa.project.dto.AddressDto;
import com.qa.project.dto.EnrichedProduct;
import com.qa.project.dto.OrderItemDto;
import com.qa.project.dto.PurchaseOrderDto;
import com.qa.project.service.OrderItemService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
@RequestMapping("/api/v1/orderItems")
@AllArgsConstructor
public class OrderItemController {
    private final OrderItemService orderItemService;
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN','ROLE_SELLER')")
    @GetMapping(path = "/getOrderItemsConfitmrdBySeller")
    ResponseEntity<List<OrderItemDto>> getOrderItemsBySellerAndStatusConfirmed(){
        log.info("Get get orderItems by seller and confirmed status");
        List<OrderItemDto> orderItemDtos = orderItemService.getOrderItemsBySellerAndStatusConfirmed();
        return ResponseEntity.ok().body(orderItemDtos);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SELLER')")
    @PutMapping("/terminateOrderItem")
    ResponseEntity<OrderItemDto> terminateOrderItem(@RequestParam(name = "id_orderItem") Long id_orderItem){
        log.info("terminate order item");
        OrderItemDto orderItemDto = orderItemService.terminateOrderItem(id_orderItem);
        return ResponseEntity.ok().body(orderItemDto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN','ROLE_SELLER')")
    @GetMapping(path = "/getLast6OrdersBySeller")
    ResponseEntity<List<OrderItemDto>> getLast6ordersBySeller(){
        log.info("Get last 6 orderItems by seller");
        List<OrderItemDto> orderItemDtos = orderItemService.getLast6OrderItems();
        return ResponseEntity.ok().body(orderItemDtos);
    }


}
