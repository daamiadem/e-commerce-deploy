package com.qa.project.controller;


import com.qa.project.dto.EnrichedBuyerProduct;
import com.qa.project.dto.EnrichedProduct;
import com.qa.project.dto.ProductDto;
import com.qa.project.service.ProductService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
@RequestMapping("/api/v1/product")
@AllArgsConstructor
public class ProductController {
    private final ProductService productService;
    @PostMapping(path = "/add_product")
    ResponseEntity<ProductDto> addNewProduct(@RequestBody ProductDto productDto, @RequestParam(name = "id_subCat") Long id_subCat ){
        log.info("Add new product");
        ProductDto productDto1 = productService.addProduct(productDto, id_subCat);
        return  ResponseEntity.ok().body(productDto1);
    }
    @PutMapping(path = "/update_product")
    ResponseEntity<ProductDto> updateProduct(@RequestBody ProductDto productDto , @RequestParam(name = "id_product") Long id_product){
        log.info("Update new product");
        ProductDto productDto1 = productService.updateProduct(productDto, id_product);
        return  ResponseEntity.ok().body(productDto1);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN','ROLE_SELLER')")
    @GetMapping(path = "/getAllProducts")
    ResponseEntity<List<EnrichedProduct>> getAllProducts(){
        log.info("Get all products if Admin or seller products if seller");
        List<EnrichedProduct> enrichedProducts = productService.getAllProducts();
        return ResponseEntity.ok().body(enrichedProducts);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_BUYER')")
    @GetMapping(path = "/getBuyerProducts")
    ResponseEntity<List<EnrichedBuyerProduct>> getProductsByBuyer(@RequestParam(name = "codeId", required = false) Long codeId){
        log.info("Get products by buyer");
        List<EnrichedBuyerProduct> enrichedProducts = productService.getProductsByBuyer(codeId);
        return ResponseEntity.ok().body(enrichedProducts);
    }
    @DeleteMapping(path = "/deleteProduct")
    ResponseEntity<Long> deleteProduct(@RequestParam(name = "id_product") Long id_product){
        log.info("delete product");
        Long deleteProduct = productService.deleteProduct(id_product);
        return  ResponseEntity.ok().body(deleteProduct);
    }
    @DeleteMapping(path = "/updateProductStatusToDeleted")
    ResponseEntity<ProductDto> updateProductStatusToDeleted(@RequestParam(name = "id_product") Long id_product){
        log.info("delete product");
        ProductDto deleteProduct = productService.UpdateProductStatusToDeleted(id_product);
        return  ResponseEntity.ok().body(deleteProduct);
    }
    @GetMapping(path = "/best-selled")
    ResponseEntity<List<ProductDto>> getBestSelledProducts(){
        log.info("Get nine best selled products");
        List<ProductDto> productDtos = productService.getBestSelledProducts();
        return ResponseEntity.ok().body(productDtos);
    }
}
