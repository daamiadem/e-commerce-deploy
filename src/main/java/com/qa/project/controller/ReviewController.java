package com.qa.project.controller;


import com.qa.project.dto.CategoryDto;
import com.qa.project.dto.ReviewDto;
import com.qa.project.service.ReviewService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
@RequestMapping("/api/v1/review")
@AllArgsConstructor
public class ReviewController {
    private final ReviewService reviewService;

    @PostMapping(path = "/add_review")
    ResponseEntity<ReviewDto> addNewReviewDto(@RequestBody ReviewDto reviewDto, @RequestParam(name = "id_product") Long id_product){
        log.info("Add new review");
        ReviewDto reviewDto1 = reviewService.addReview(reviewDto, id_product);
        return  ResponseEntity.ok().body(reviewDto1);
    }
    @GetMapping(path = "/getAllReviewsByProduct")
    ResponseEntity<List<ReviewDto>> getAllReviewByProduct(@RequestParam(name = "id_product") Long id_product){
        log.info("Get all review by product");
        List<ReviewDto> reviewByProduct = reviewService.getReviewByProduct(id_product);
        return ResponseEntity.ok().body(reviewByProduct);
    }
    @DeleteMapping(path = "/deleteReview")
    ResponseEntity<Long> deleteSubCategory(@RequestParam(name = "id_review") Long id_review){
        log.info("delete  review");
        Long deletedReview = reviewService.deleteReview(id_review);
        return  ResponseEntity.ok().body(deletedReview);
    }
}
