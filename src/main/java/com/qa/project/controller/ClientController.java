package com.qa.project.controller;


import com.qa.project.dto.ClientDto;
import com.qa.project.dto.EnrichedBuyer;
import com.qa.project.dto.EnrichedSeller;
import com.qa.project.service.ClientService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
@RequestMapping("/api/v1/client")
@AllArgsConstructor

public class ClientController {
    private final ClientService clientService ;


    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
    @GetMapping(path = "/getAllClients")

    ResponseEntity<List<ClientDto>> getAllClient(){
        log.info("Get all client");
        List<ClientDto> allClients = clientService.getAllClients();
        return ResponseEntity.ok().body(allClients);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
    @GetMapping(path = "/getClient")
    ResponseEntity<ClientDto> getClientById(@RequestParam(name = "id_client") Long IdClient){
        log.info("Get detail client");
        ClientDto clientDto = clientService.getClientById(IdClient);
        return  ResponseEntity.ok().body(clientDto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
    @DeleteMapping(path ="/deleteClient")
    ResponseEntity<Long> deleteClient(@RequestParam(name = "id_client") Long IdClient){
        log.info("delete client");
        Long deletedClient = clientService.deleteClient(IdClient);
        return ResponseEntity.ok().body(deletedClient);
    }


//    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
    @GetMapping(path = "/enriched-buyers")
    ResponseEntity<List<EnrichedBuyer>> getAllEnrichedBuyers(){
        log.info("Get all enriched buyers");
        List<EnrichedBuyer> allClients = clientService.getAllEnrichedBuyers();
        return ResponseEntity.ok().body(allClients);
    }
    @GetMapping(path = "/enriched-sellers")
    ResponseEntity<List<EnrichedSeller>> getAllEnrichedSellers(){
        log.info("Get all enriched sellers");
        List<EnrichedSeller> allClients = clientService.getAllEnrichedSeller();
        return ResponseEntity.ok().body(allClients);
    }
    @PutMapping(path = "/changeSellerStatus")
    ResponseEntity<ClientDto> changeSellerStatus(@RequestBody ClientDto clientDto){
        log.info("Change seller status");
        ClientDto clientDto1 = clientService.changeSellerStatus(clientDto.getId());
        return ResponseEntity.ok().body(clientDto1);
    }









}
