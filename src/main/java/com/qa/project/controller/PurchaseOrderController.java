package com.qa.project.controller;

import com.qa.project.dto.AddressDto;
import com.qa.project.dto.OrderItemDto;
import com.qa.project.dto.ProductDto;
import com.qa.project.dto.PurchaseOrderDto;
import com.qa.project.service.PurchaseOrderService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/purchase-order")
@AllArgsConstructor
@Slf4j
public class PurchaseOrderController {
    private final PurchaseOrderService purchaseOrderService;

    @GetMapping("/find-all")
//    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
    ResponseEntity<List<PurchaseOrderDto>> getAll(){
        log.info("get all orders by admin");
        List<PurchaseOrderDto> purchaseOrderDtoList = purchaseOrderService.getAll();
        return ResponseEntity.ok().body(purchaseOrderDtoList);
    }

    @GetMapping("/getpurchaseOrderByProduct")
//    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
    ResponseEntity<List<OrderItemDto>> getPurchaseOrderByProduct(@RequestBody ProductDto productDto){
        log.info("get purchase order by product");
        List<OrderItemDto> orderDtoList = purchaseOrderService.getOrderItemsByProduct(productDto);
        return ResponseEntity.ok().body(orderDtoList);
    }

    @PostMapping(path = "/add_purchaseOrder")
    ResponseEntity<PurchaseOrderDto> addNewPurchaseOrder(@RequestBody OrderItemDto orderItemDto ){
        log.info("Add new PurchaseOrder");
        PurchaseOrderDto purchaseOrder = purchaseOrderService.createPurchaseOrder(orderItemDto);
        return  ResponseEntity.ok().body(purchaseOrder);
    }
    @GetMapping("/getpurchaseOrderOpenedByClient")
//    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
    ResponseEntity<PurchaseOrderDto> getPurchaseOrderOpenedByProduct(){
        log.info("get purchase order opened By Client");
        PurchaseOrderDto openedPurchaseOrderByBuyer = purchaseOrderService.getOpenedPurchaseOrderByBuyer();
        return ResponseEntity.ok().body(openedPurchaseOrderByBuyer);
    }

@PutMapping("/confirmPurchaseOrder")
ResponseEntity<PurchaseOrderDto> confirmPurchaseOrder(@RequestBody AddressDto addressDto, @RequestParam(name = "id_purchaseOrder") Long id_purchaseOrder){
    log.info("confirm purchase Order");
    PurchaseOrderDto purchaseOrderDto = purchaseOrderService.confirmBuyPruchaseOrder(addressDto, id_purchaseOrder);
    return ResponseEntity.ok().body(purchaseOrderDto);
}

    @DeleteMapping(path = "/deletePurchaseOrder")
    ResponseEntity<Long> deletePurchaseOrder(@RequestParam(name = "id_purchaseOrder") Long id_purchaseOrder){
        log.info("Add new purchase order");
        Long deletedPurchaseOrder = purchaseOrderService.cancelPurchaseOrder(id_purchaseOrder);
        return  ResponseEntity.ok().body(deletedPurchaseOrder);
    }


}
