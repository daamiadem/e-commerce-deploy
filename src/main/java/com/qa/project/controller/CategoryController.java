package com.qa.project.controller;

import com.qa.project.dto.CategoryDto;
import com.qa.project.dto.ClientDto;
import com.qa.project.dto.SubCategoryDto;
import com.qa.project.service.CategoryService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
@RequestMapping("/api/v1/category")
@AllArgsConstructor
public class CategoryController {
    private final CategoryService categoryService;

    @GetMapping(path = "/getAllCategpries")
    ResponseEntity<List<CategoryDto>> getAllCategories(){
        log.info("Get all Categories");
        List<CategoryDto> categoryDtos = categoryService.getAllCategoriesDto();
        return ResponseEntity.ok().body(categoryDtos);
    }

    @PostMapping(path = "/addCategory")
    ResponseEntity<CategoryDto> addNewCategory(@RequestBody CategoryDto categoryDto){
        log.info("Add new category");
        CategoryDto category = categoryService.addCategory(categoryDto);
        return  ResponseEntity.ok().body(category);
    }

    @DeleteMapping(path = "/deleteCategory")
    ResponseEntity<Long> deleteSubCategory(@RequestParam(name = "id_category") Long id_category){
        log.info("Add new category");
        Long deletedCategory = categoryService.deleteCategory(id_category);
        return  ResponseEntity.ok().body(deletedCategory);
    }


}
