package com.qa.project.repository;


import com.qa.project.model.security.Myuser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;
@Repository
public interface MyuserRepository extends JpaRepository<Myuser, Long> {
    @Query("SELECT u FROM myuser u WHERE u.username = :username")
    Optional<Myuser> findByUsername(String username);
}