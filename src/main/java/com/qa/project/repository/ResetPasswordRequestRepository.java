package com.qa.project.repository;

import com.qa.project.model.ResetPasswordRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ResetPasswordRequestRepository extends JpaRepository<ResetPasswordRequest, Long> {
    Optional<ResetPasswordRequest> findResetPasswordRequestByClientId(Long id);
    Optional<ResetPasswordRequest> findResetPasswordRequestByVerificationToken(String token);
}
