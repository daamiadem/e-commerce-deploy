package com.qa.project.repository;

import com.qa.project.dto.OrderStatus;
import com.qa.project.model.OrderItem;
import com.qa.project.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderItemRepository extends JpaRepository<OrderItem, Long> {
    List<OrderItem> findByProduct_SubCategory_Id(Long idSubCategory);
    List<OrderItem> findByProductId(Long id);
    List<OrderItem> findAllByProductIdAndOrderStatus(Long id, OrderStatus status);
    List<OrderItem> findByProduct(Product product);
}
