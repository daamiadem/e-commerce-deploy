package com.qa.project.repository;

import com.qa.project.model.Client;
import com.qa.project.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    List<Product> findBySeller(Client client);
    List<Product> findAllBySubCategory_id(Long id);
}
