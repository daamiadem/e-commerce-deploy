package com.qa.project.repository;

import com.qa.project.dto.CategoryDto;
import com.qa.project.model.Category;
import com.qa.project.model.SubCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {
    Category findBySubCategoriesContaining(SubCategory subCategory);
}
