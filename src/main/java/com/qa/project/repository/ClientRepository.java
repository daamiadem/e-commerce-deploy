package com.qa.project.repository;

import com.qa.project.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {

Optional<Client> findClientById(Long ClientId);

    Client findClientByEmail(String email);


}
