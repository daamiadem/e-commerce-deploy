package com.qa.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.ApplicationPidFileWriter;

@EntityScan("com.qa.project.model")
@SpringBootApplication
public class EcommerceApplication {

	public static void main(String[] args) {
		SpringApplication springApplication = new SpringApplication(EcommerceApplication.class);
		springApplication.addListeners(new ApplicationPidFileWriter());
		SpringApplication.run(EcommerceApplication.class, args);
	}
}
