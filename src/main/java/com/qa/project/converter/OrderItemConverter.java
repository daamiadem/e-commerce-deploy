package com.qa.project.converter;

import com.qa.project.dto.OrderItemDto;
import com.qa.project.model.OrderItem;
import lombok.Data;
import org.springframework.core.convert.converter.Converter;

import java.util.Objects;

@Data(staticConstructor = "newInstance")
public class OrderItemConverter implements Converter<OrderItem, OrderItemDto> {
    @Override
    public OrderItemDto convert(OrderItem orderItem){
        if (Objects.isNull(orderItem)){
            return null;
        }
        return OrderItemDto.builder()
                .id(orderItem.getId())
                .quantity(orderItem.getQuantity())
                .productDto(ProductConverter.newInstance().convert(orderItem.getProduct()))
                .unitPrice(orderItem.getUnitPrice())
                .orderStatus(orderItem.getOrderStatus())
                .build();
    }
}
