package com.qa.project.converter;

import com.qa.project.dto.CategoryDto;
import com.qa.project.model.Category;
import lombok.Data;
import org.springframework.core.convert.converter.Converter;

import java.util.Objects;
import java.util.stream.Collectors;

@Data(staticConstructor = "newInstance")
public class CategoryDtoConverter implements Converter<CategoryDto, Category> {
    @Override
    public Category convert(CategoryDto categoryDto){
        if (Objects.isNull(categoryDto)){
            return null;
        }
        return Category.builder()
                .id(categoryDto.getId())
                .name(categoryDto.getName())
                .subCategories(Objects.nonNull(categoryDto.getSubCategoryDto())?categoryDto.getSubCategoryDto().stream().map(subCategoryDto -> SubCategoryDtoConverter.newInstance().convert(subCategoryDto)).collect(Collectors.toList()) : null)
                .build();
    }
}
