package com.qa.project.converter;

import com.qa.project.dto.PurchaseOrderDto;
import com.qa.project.model.PurchaseOrder;
import lombok.Data;
import org.springframework.core.convert.converter.Converter;

import java.util.Objects;
import java.util.stream.Collectors;

@Data(staticConstructor = "newInstance")
public class PurchaseOrderDtoConverter implements Converter<PurchaseOrderDto, PurchaseOrder> {
    @Override
    public PurchaseOrder convert(PurchaseOrderDto purchaseOrderDto){
        if (Objects.isNull(purchaseOrderDto)){
            return null;
        }
        return PurchaseOrder.builder()
                .id(purchaseOrderDto.getId())
                .orderDate(purchaseOrderDto.getOrderDate())
                .client(ClientDtoConverter.newInstance().convert(purchaseOrderDto.getUserDto()))
                .shippingAddress(AddressDtoConverter.newInstance().convert(purchaseOrderDto.getShippingAddressDto()))
                .totalAmount(purchaseOrderDto.getTotalAmount())
                .purchaseOrderStatus(purchaseOrderDto.getPurchaseOrderStatus())
                .orderItems(Objects.nonNull(purchaseOrderDto.getOrderItemDtos())?purchaseOrderDto.getOrderItemDtos().stream().map(orderItemDto -> OrderItemDtoConverter.newInstance().convert(orderItemDto)).collect(Collectors.toList()) : null)
                .build();
    }
}
