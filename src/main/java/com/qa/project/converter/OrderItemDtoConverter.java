package com.qa.project.converter;

import com.qa.project.dto.OrderItemDto;
import com.qa.project.model.OrderItem;
import lombok.Data;
import org.springframework.core.convert.converter.Converter;

import java.util.Objects;

@Data(staticConstructor = "newInstance")
public class OrderItemDtoConverter implements Converter<OrderItemDto, OrderItem> {
    @Override
    public OrderItem convert(OrderItemDto orderItemDto){
        if (Objects.isNull(orderItemDto)){
            return null;
        }
        return OrderItem.builder()
                .id(orderItemDto.getId())
                .quantity(orderItemDto.getQuantity())
                .product(ProductDtoConverter.newInstance().convert(orderItemDto.getProductDto()))
                .unitPrice(orderItemDto.getUnitPrice())
                .orderStatus(orderItemDto.getOrderStatus())
                .build();

    }
}
