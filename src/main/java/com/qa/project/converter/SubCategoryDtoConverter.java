package com.qa.project.converter;

import com.qa.project.dto.SubCategoryDto;
import com.qa.project.model.SubCategory;
import lombok.Data;
import org.springframework.core.convert.converter.Converter;

import java.util.Objects;

@Data(staticConstructor = "newInstance")
public class SubCategoryDtoConverter implements Converter<SubCategoryDto, SubCategory> {
    @Override
    public SubCategory convert(SubCategoryDto subCategoryDto){
        if (Objects.isNull(subCategoryDto)){
            return null;
        }
        return SubCategory.builder()
                .id(subCategoryDto.getId())
                .name(subCategoryDto.getName())
                .build();
    }
}
