package com.qa.project.converter;

import com.qa.project.dto.ReviewDto;
import com.qa.project.model.Review;
import lombok.Data;
import org.springframework.core.convert.converter.Converter;

import java.util.Objects;

@Data(staticConstructor = "newInstance")
public class ReviewConverter implements Converter<Review, ReviewDto> {
    @Override
    public ReviewDto convert(Review review){
        if (Objects.isNull(review)){
            return null;
        }
        return ReviewDto.builder()
                .id(review.getId())
                .comment(review.getComment())
                .productDto(ProductConverter.newInstance().convert(review.getProduct()))
                .rating(review.getRating())
                .userDto(ClientConverter.newInstance().convert(review.getClient()))
                .build();
    }
}
