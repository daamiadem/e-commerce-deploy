package com.qa.project.converter;

import com.qa.project.dto.CategoryDto;
import com.qa.project.model.Category;
import lombok.Data;
import org.springframework.core.convert.converter.Converter;

import java.util.Objects;
import java.util.stream.Collectors;

@Data(staticConstructor = "newInstance")
public class CategoryConverter implements Converter<Category, CategoryDto> {
    @Override
    public CategoryDto convert(Category category){
        if (Objects.isNull(category)){
            return null;
        }
        return CategoryDto.builder()
                .id(category.getId())
                .name(category.getName())
                .subCategoryDto(Objects.nonNull(category.getSubCategories())?category.getSubCategories().stream().map(subCategory -> SubCategoryConverter.newInstance().convert(subCategory)).collect(Collectors.toList()) : null)
                .build();
    }
}
