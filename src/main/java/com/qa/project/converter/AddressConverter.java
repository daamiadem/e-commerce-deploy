package com.qa.project.converter;

import com.qa.project.dto.AddressDto;
import com.qa.project.model.Address;
import lombok.Data;
import org.springframework.core.convert.converter.Converter;

import java.util.Objects;

@Data(staticConstructor = "newInstance")
public class AddressConverter implements Converter<Address, AddressDto> {
    @Override
    public AddressDto convert(Address address){
        if (Objects.isNull(address)){
            return null;
        }
        return AddressDto.builder()
                .id(address.getId())
                .city(address.getCity())
                .country(address.getCountry())
                .postalCode(address.getPostalCode())
                .street(address.getStreet())
                .build();
    }
}
