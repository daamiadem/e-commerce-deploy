package com.qa.project.converter;

import com.qa.project.dto.PurchaseOrderDto;
import com.qa.project.model.PurchaseOrder;
import lombok.Data;
import org.springframework.core.convert.converter.Converter;

import java.util.Objects;
import java.util.stream.Collectors;

@Data(staticConstructor = "newInstance")
public class PurchaseOrderConverter implements Converter<PurchaseOrder, PurchaseOrderDto> {
    @Override
    public PurchaseOrderDto convert(PurchaseOrder purchaseOrder){
        if (Objects.isNull(purchaseOrder)){
            return null;
        }
        return PurchaseOrderDto.builder()
                .id(purchaseOrder.getId())
                .orderDate(purchaseOrder.getOrderDate())
                .shippingAddressDto(AddressConverter.newInstance().convert(purchaseOrder.getShippingAddress()))
                .totalAmount(purchaseOrder.getTotalAmount())
                .userDto(ClientConverter.newInstance().convert(purchaseOrder.getClient()))
                .purchaseOrderStatus(purchaseOrder.getPurchaseOrderStatus())
                .orderItemDtos(Objects.nonNull(purchaseOrder.getOrderItems())?purchaseOrder.getOrderItems().stream().map(orderItem -> OrderItemConverter.newInstance().convert(orderItem)).collect(Collectors.toList()):null)
                .build();
    }
}
