package com.qa.project.converter;

import com.qa.project.dto.ClientDto;
import com.qa.project.model.Client;
import lombok.Data;
import org.springframework.core.convert.converter.Converter;
import java.util.Objects;

@Data(staticConstructor = "newInstance")
public class ClientDtoConverter implements Converter<ClientDto, Client> {
    @Override
    public Client convert(ClientDto clientDto){
        if (Objects.isNull(clientDto)){
            return null;
        }
        return Client.builder()
                .id(clientDto.getId())
                .nom(clientDto.getNom())
                .prenom(clientDto.getPrenom())
                .email(clientDto.getEmail())
                .dateNaissance(clientDto.getDateNaissance())
                .dateCreationCompte(clientDto.getDateCreationCompte())
                .pays(clientDto.getPays())
                .verified(clientDto.getVerified())
                .phoneNumber(clientDto.getPhoneNumber())
                .build();
    }
}
