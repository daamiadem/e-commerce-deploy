package com.qa.project.converter;

import com.qa.project.dto.AddressDto;
import com.qa.project.model.Address;
import lombok.Data;
import org.springframework.core.convert.converter.Converter;

import java.util.Objects;

@Data(staticConstructor = "newInstance")
public class AddressDtoConverter implements Converter<AddressDto, Address> {
    @Override
    public Address convert(AddressDto addressDto){
        if (Objects.isNull(addressDto)){
            return null;
        }
        return Address.builder()
                .id(addressDto.getId())
                .city(addressDto.getCity())
                .country(addressDto.getCountry())
                .postalCode(addressDto.getPostalCode())
                .street(addressDto.getStreet())
                .build();
    }
}
