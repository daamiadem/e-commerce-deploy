package com.qa.project.converter;


import com.qa.project.dto.ClientDto;
import com.qa.project.model.Client;
import lombok.Data;
import org.springframework.core.convert.converter.Converter;
import java.util.Objects;

@Data(staticConstructor = "newInstance")
public class ClientConverter implements Converter<Client, ClientDto> {

    @Override
    public ClientDto convert(Client client){
        if (Objects.isNull(client)){
            return null;
        }
        return ClientDto.builder()
                .id(client.getId())
                .nom(client.getNom())
                .prenom(client.getPrenom())
                .email(client.getEmail())
                .dateCreationCompte(client.getDateCreationCompte())
                .dateNaissance(client.getDateNaissance())
                .pays(client.getPays())
                .verified(client.getVerified())
                .phoneNumber(client.getPhoneNumber())
                .build();
            }
}
