package com.qa.project.converter;

import com.qa.project.dto.ReviewDto;
import com.qa.project.model.Review;
import lombok.Data;
import org.springframework.core.convert.converter.Converter;

import java.util.Objects;

@Data(staticConstructor = "newInstance")
public class ReviewDtoConverter implements Converter<ReviewDto, Review> {
    @Override
    public Review convert(ReviewDto reviewDto){
        if (Objects.isNull(reviewDto)){
            return null;
        }
        return Review.builder()
                .id(reviewDto.getId())
                .client(ClientDtoConverter.newInstance().convert(reviewDto.getUserDto()))
                .product(ProductDtoConverter.newInstance().convert(reviewDto.getProductDto()))
                .rating(reviewDto.getRating())
                .comment(reviewDto.getComment())
                .build();
    }
}
