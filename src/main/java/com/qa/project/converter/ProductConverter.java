package com.qa.project.converter;

import com.qa.project.dto.ProductDto;
import com.qa.project.model.Product;
import lombok.Data;
import org.springframework.core.convert.converter.Converter;

import java.util.Objects;

@Data(staticConstructor = "newInstance")
public class ProductConverter implements Converter<Product, ProductDto> {
    @Override
    public ProductDto convert(Product product){
        if (Objects.isNull(product)){
            return null;
        }
        return ProductDto.builder()
                .id(product.getId())
                .name(product.getName())
                .description(product.getDescription())
                .price(product.getPrice())
                .productStatus(product.getProductStatus())
                .quantity(product.getQuantity())
                .subCategoryDto(SubCategoryConverter.newInstance().convert(product.getSubCategory()))
                .sellerDto(ClientConverter.newInstance().convert(product.getSeller()))
                .image(product.getImage())
                .build();
    }
}
