package com.qa.project.converter;

import com.qa.project.dto.ProductDto;
import com.qa.project.model.Product;
import lombok.Data;
import org.springframework.core.convert.converter.Converter;

import java.util.Objects;

@Data(staticConstructor = "newInstance")
public class ProductDtoConverter implements Converter<ProductDto, Product> {
    @Override
    public Product convert(ProductDto productDto){
        if (Objects.isNull(productDto)){
            return null;
        }
        return Product.builder()
                .id(productDto.getId())
                .name(productDto.getName())
                .productStatus(productDto.getProductStatus())
                .description(productDto.getDescription())
                .price(productDto.getPrice())
                .quantity(productDto.getQuantity())
                .subCategory(SubCategoryDtoConverter.newInstance().convert(productDto.getSubCategoryDto()))
                .seller(ClientDtoConverter.newInstance().convert(productDto.getSellerDto()))
                .image(productDto.getImage())
                .build();
    }
}
