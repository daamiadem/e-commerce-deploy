package com.qa.project.converter;

import com.qa.project.dto.SubCategoryDto;
import com.qa.project.model.SubCategory;
import lombok.Data;
import org.springframework.core.convert.converter.Converter;

import java.util.Objects;

@Data(staticConstructor = "newInstance")
public class SubCategoryConverter implements Converter<SubCategory, SubCategoryDto> {
    @Override
    public SubCategoryDto convert(SubCategory subCategory){
        if (Objects.isNull(subCategory)){
            return null;
        }
        return SubCategoryDto.builder()
                .id(subCategory.getId())
                .name(subCategory.getName())
                .build();
    }
}
