package com.qa.project.service;

import com.qa.project.converter.CategoryConverter;
import com.qa.project.converter.CategoryDtoConverter;
import com.qa.project.dto.CategoryDto;
import com.qa.project.model.Category;
import com.qa.project.repository.CategoryRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class CategoryService {
private final CategoryRepository categoryRepository;

public List<CategoryDto> getAllCategoriesDto(){
    List<Category> categories = categoryRepository.findAll();
    return categories.stream().map(category -> CategoryConverter.newInstance().convert(category)).collect(Collectors.toList());
}
public CategoryDto addCategory(CategoryDto categoryDto){
    Category category = CategoryDtoConverter.newInstance().convert(categoryDto);
    Category categorysaved = categoryRepository.save(category);
    return CategoryConverter.newInstance().convert(category);
}
public Long deleteCategory(Long idCategory){
    categoryRepository.deleteById(idCategory);
    return idCategory;
}

}
