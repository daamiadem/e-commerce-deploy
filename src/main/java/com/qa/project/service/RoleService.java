package com.qa.project.service;


import com.qa.project.model.security.Myuser;
import com.qa.project.model.security.Role;
import com.qa.project.repository.MyuserRepository;
import com.qa.project.repository.RoleRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class RoleService {

    private final RoleRepository roleRepository;
    private final MyuserRepository myuserRepository;

    public Role getRoleByUsername(String username){

        Optional<Myuser> myuser = myuserRepository.findByUsername(username);
        Role role = roleRepository.findByUsersId(myuser.get().getId());
        return role;
    }
}
