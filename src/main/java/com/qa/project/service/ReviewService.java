package com.qa.project.service;

import com.qa.project.converter.ReviewConverter;
import com.qa.project.converter.ReviewDtoConverter;
import com.qa.project.dto.ReviewDto;
import com.qa.project.model.Client;
import com.qa.project.model.Product;
import com.qa.project.model.Review;
import com.qa.project.repository.ClientRepository;
import com.qa.project.repository.ProductRepository;
import com.qa.project.repository.ReviewRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ReviewService {
    private final ReviewRepository reviewRepository;
    private final ClientRepository clientRepository;
    private final ProductRepository productRepository;

    public ReviewDto addReview(ReviewDto reviewDto , Long IdProduct){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String email = new String();
        if (authentication != null && authentication.isAuthenticated()) {
            email = authentication.getName();
        }
        Client client = clientRepository.findClientByEmail(email);
        Product product = productRepository.findById(IdProduct).orElse(null);
        Review review= ReviewDtoConverter.newInstance().convert(reviewDto);
        review.setClient(client);
        review.setProduct(product);
        Review reviewSaved = reviewRepository.save(review);
        return ReviewConverter.newInstance().convert(reviewSaved);
    }
    public List<ReviewDto> getReviewByProduct(Long idProduct){
        Product product = productRepository.findById(idProduct).orElse(null);
        List<Review> reviewsByProduct = reviewRepository.findByProduct(product);
        return reviewsByProduct.stream().map(review -> ReviewConverter.newInstance().convert(review)).collect(Collectors.toList());

    }
    public Long deleteReview(Long idReview){
        reviewRepository.deleteById(idReview);
        return idReview;
    }
}
