package com.qa.project.service;


import com.qa.project.converter.ClientConverter;
import com.qa.project.converter.ClientDtoConverter;
import com.qa.project.converter.ProductConverter;
import com.qa.project.dto.ClientDto;
import com.qa.project.dto.EnrichedBuyer;
import com.qa.project.dto.EnrichedSeller;
import com.qa.project.model.Client;
import com.qa.project.model.OrderItem;
import com.qa.project.model.PurchaseOrder;
import com.qa.project.repository.ClientRepository;
import com.qa.project.repository.ProductRepository;
import com.qa.project.repository.PurchaseOrderRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.*;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor

public class ClientService {
    private final ClientRepository clientRepository;
    private final  RoleService roleService;
    private final  PurchaseOrderService purchaseOrderService;
    private final ProductRepository productRepository;
    private final PurchaseOrderRepository purchaseOrderRepository;



    public ClientDto getClientById(Long id){
        Client client = this.clientRepository.findClientById(id).orElseThrow(() -> new IllegalStateException("no client for that id"));
        return ClientConverter.newInstance().convert(client);
    }

    public ClientDto create(ClientDto clientDto){
        if(Objects.nonNull(clientDto)){
            Client client = ClientDtoConverter.newInstance().convert(clientDto);
            this.clientRepository.save(client);
            return clientDto;
        }else {
            return null;
        }
    }

    public List<ClientDto> getAllClients (){
        List<Client> clients = clientRepository.findAll();
        List<ClientDto> clientDtos = clients.stream().map(client -> ClientConverter.newInstance().convert(client)).collect(Collectors.toList());
        return  clientDtos;

    }

    public Long deleteClient(Long idClient){
        Client client = clientRepository.findClientById(idClient).orElse(null);
        if(Objects.nonNull(client)){
            clientRepository.deleteById(idClient);
            return idClient;
        }
        else return null ;
    }

    public ClientDto getClientByEmail(String username) {
        Client client = clientRepository.findClientByEmail(username);
        return ClientConverter.newInstance().convert(client);
    }


    public List<EnrichedBuyer> getAllEnrichedBuyers() {
        List<Client> buyers = clientRepository.findAll();
        List<EnrichedBuyer> enrichedBuyers = buyers.stream().filter(client -> roleService.getRoleByUsername(client.getEmail()).getName().equals("ROLE_BUYER")).map(client ->
             EnrichedBuyer.builder().clientDto(ClientConverter.newInstance().convert(client)).orders(purchaseOrderService.getPurchaseOrderByClient(ClientConverter.newInstance().convert(client))).build()
        ).collect(Collectors.toList());
        return enrichedBuyers;
    }

    public List<EnrichedSeller> getAllEnrichedSeller(){
        List<Client> sellers = clientRepository.findAll();
        List<EnrichedSeller> enrichedSellers = sellers.stream().filter(client -> roleService.getRoleByUsername(client.getEmail()).getName().equals("ROLE_SELLER")).map(seller ->
                EnrichedSeller.builder().sellerDto(ClientConverter.newInstance().convert(seller)).productDtos(productRepository.findBySeller(seller).stream().map(product -> ProductConverter.newInstance().convert(product)).collect(Collectors.toList())).balance(getBalanceBySellerId(seller.getId())).build()
        ).collect(Collectors.toList());
        return enrichedSellers;
    }
    public Long getBalanceBySellerId(Long id){
        Long amount = 0L;
        List<PurchaseOrder> allOrders = purchaseOrderRepository.findAll();
        for (PurchaseOrder purchaseOrder : allOrders) {
            for (OrderItem orderItem : purchaseOrder.getOrderItems()) {
                if (orderItem.getProduct().getSeller().getId().equals(id)){
                    amount +=  (orderItem.getProduct().getPrice() * orderItem.getQuantity());
                }
            }
        }
        return amount;
    }

    public ClientDto changeSellerStatus(Long id) {
        Optional<Client> client = clientRepository.findClientById(id);
        if (client.isPresent()){
            Client client1 = client.get();
            client1.setVerified(!client1.getVerified());
            clientRepository.save(client1);
            return ClientConverter.newInstance().convert(client1);
        }
        return null;
    }
}
