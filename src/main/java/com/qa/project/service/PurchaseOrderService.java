package com.qa.project.service;

import com.qa.project.converter.*;
import com.qa.project.dto.*;
import com.qa.project.model.*;
import com.qa.project.repository.ClientRepository;
import com.qa.project.repository.OrderItemRepository;
import com.qa.project.repository.ProductRepository;
import com.qa.project.repository.PurchaseOrderRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class PurchaseOrderService {
    private final PurchaseOrderRepository purchaseOrderRepository;
    private final ClientRepository clientRepository;
    private final OrderItemRepository orderItemRepository;
    private final ProductRepository productRepository;


    public List<PurchaseOrderDto> getAll() {
        List<PurchaseOrder> purchaseOrders = purchaseOrderRepository.findAll();
        if (Objects.nonNull(purchaseOrders) && purchaseOrders.size() != 0){
            return purchaseOrders.stream().map(purchaseOrder -> PurchaseOrderConverter.newInstance().convert(purchaseOrder)).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }
    public List<PurchaseOrderDto> getPurchaseOrderByClient(ClientDto clientDto){
        Client client = ClientDtoConverter.newInstance().convert(clientDto);
        List<PurchaseOrder> purchaseOrderByClient = purchaseOrderRepository.findByClient(client);
        return  purchaseOrderByClient.stream().map(purchaseOrder -> PurchaseOrderConverter.newInstance().convert(purchaseOrder)).collect(Collectors.toList());
    }

    public List<OrderItemDto> getOrderItemsByProduct(ProductDto productDto){
        Product product = ProductDtoConverter.newInstance().convert(productDto);
        return orderItemRepository.findByProduct(product).stream().map(orderItem -> OrderItemConverter.newInstance().convert(orderItem)).collect(Collectors.toList());
    }
    public LocalDateTime convertToLocalDateViaInstant(Date dateToConvert) {
        return Instant.ofEpochMilli(dateToConvert.getTime())
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
    }
    public PurchaseOrderDto createPurchaseOrder(OrderItemDto orderItemDto) {
        OrderItem orderItem = OrderItemDtoConverter.newInstance().convert(orderItemDto);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object principal = authentication.getPrincipal();
        if (principal instanceof UserDetails) {
            String email = ((UserDetails) principal).getUsername();
            Client client = clientRepository.findClientByEmail(email);
            List<PurchaseOrder> purchaseOrderyByClient = purchaseOrderRepository.findByClient(client);
            PurchaseOrder purchaseOrderOpened = purchaseOrderyByClient.stream().filter(purchaseOrder -> purchaseOrder.getPurchaseOrderStatus().equals(PurchaseOrderStatus.OPENED)).findFirst().orElse(null);
            if (Objects.nonNull(purchaseOrderOpened)) {
                List<OrderItem> orderItems = purchaseOrderOpened.getOrderItems();
                orderItems.add(orderItem);
                List<OrderItem> orderItemListUpdated = orderItems.stream().map(orderItem1 -> {
                    orderItem1.setOrderStatus(OrderStatus.PENDING);
                    return orderItem1;
                }).collect(Collectors.toList());
                purchaseOrderOpened.setOrderItems(orderItemListUpdated);
                PurchaseOrder saved = purchaseOrderRepository.save(purchaseOrderOpened);
                return PurchaseOrderConverter.newInstance().convert(saved);
            }
            else {
                Date date = new Date();
                //assert orderItem != null;
                orderItem.setOrderStatus(OrderStatus.PENDING);
                BigDecimal totalAmount = orderItem.getUnitPrice().multiply(new BigDecimal(orderItem.getQuantity()));
                List<OrderItem> orderItems = new ArrayList<>();
                orderItems.add(orderItem);
                PurchaseOrder purchaseOrderBuilded = PurchaseOrder.builder()
                        .purchaseOrderStatus(PurchaseOrderStatus.OPENED)
                        .orderDate(convertToLocalDateViaInstant(date))
                        .client(client)
                        .totalAmount(totalAmount)
                        .orderItems(orderItems)
                        .build();
                PurchaseOrder saved = purchaseOrderRepository.save(purchaseOrderBuilded);
                return PurchaseOrderConverter.newInstance().convert(saved);

            }

        }
        return null ;
    }

    public PurchaseOrderDto getOpenedPurchaseOrderByBuyer() {
        List<PurchaseOrder> byPurchaseOrderStatus = purchaseOrderRepository.findByPurchaseOrderStatus(PurchaseOrderStatus.OPENED);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object principal = authentication.getPrincipal();
        if (principal instanceof UserDetails) {
            String email = ((UserDetails) principal).getUsername();
            Client client = clientRepository.findClientByEmail(email);
            PurchaseOrder purchaseOrderByClient = byPurchaseOrderStatus.stream().filter(purchaseOrder -> purchaseOrder.getClient().equals(client)).findFirst().orElse(null);
            return PurchaseOrderConverter.newInstance().convert(purchaseOrderByClient);
        }
        return null;
    }

    public PurchaseOrderDto confirmBuyPruchaseOrder(AddressDto addressDto, Long IdPurchaseOrder) {
       // Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Address address =AddressDtoConverter.newInstance().convert(addressDto);
        Date date = new Date();
      //  Object principal = authentication.getPrincipal();
        /*if (principal instanceof UserDetails) {
            String email = ((UserDetails) principal).getUsername();
            Client client = clientRepository.findClientByEmail(email);*/
            //List<PurchaseOrder> purchaseOrderyByClient = purchaseOrderRepository.findByClient(client);
           // PurchaseOrder purchaseOrderOpened = purchaseOrderyByClient.stream().filter(purchaseOrder -> purchaseOrder.getPurchaseOrderStatus().equals(PurchaseOrderStatus.OPENED)).findFirst().orElse(null);
            PurchaseOrder purchaseOrderOpened = purchaseOrderRepository.findById(IdPurchaseOrder).orElse(null);
            if (Objects.nonNull(purchaseOrderOpened)) {
                purchaseOrderOpened.setPurchaseOrderStatus(PurchaseOrderStatus.CONFIRMED);
                List<OrderItem> orderItems = purchaseOrderOpened.getOrderItems();
                List<OrderItem> savedOrderItemList = orderItems.stream().map(orderItem -> {
                    if (orderItem.getProduct().getQuantity() >= orderItem.getQuantity()) {
                        orderItem.setOrderStatus(OrderStatus.CONFIRMED);
                        Product product = orderItem.getProduct();
                        product.setQuantity(product.getQuantity() - orderItem.getQuantity());
                        if (product.getQuantity() < 10) {
                            product.setProductStatus(ProductStatus.LOWSTOCK);
                        } else if (product.getQuantity() == 0) {
                            product.setProductStatus(ProductStatus.OUTOFSTOCK);
                        }
                        Product savedProduct = productRepository.save(product);
                        orderItem.setProduct(savedProduct);
                        OrderItem saveOrderItem = orderItemRepository.save(orderItem);
                        return saveOrderItem;
                    }
                    return null;
                }).collect(Collectors.toList());
                purchaseOrderOpened.setOrderItems(savedOrderItemList);
                purchaseOrderOpened.setShippingAddress(address);
                purchaseOrderOpened.setOrderDate(convertToLocalDateViaInstant(date));
                PurchaseOrder savedPurchaseOrder = purchaseOrderRepository.save(purchaseOrderOpened);
                return PurchaseOrderConverter.newInstance().convert(savedPurchaseOrder);


        }
        return null;
    }

public Long cancelPurchaseOrder( Long idPurchaseOrder) {
    PurchaseOrder purchaseOrder = purchaseOrderRepository.findById(idPurchaseOrder).orElse(null);
    if (purchaseOrder != null && purchaseOrder.getPurchaseOrderStatus().equals(PurchaseOrderStatus.OPENED)) {
        List<OrderItem> orderItems = purchaseOrder.getOrderItems();
        List<OrderItem> orderItemList = orderItems.stream().map(orderItem -> {
            orderItem.setOrderStatus(OrderStatus.CANCELLED);
            return orderItem;
        }).collect(Collectors.toList());
        orderItemRepository.saveAll(orderItemList);
    }
    if (purchaseOrder != null && purchaseOrder.getPurchaseOrderStatus().equals(PurchaseOrderStatus.CONFIRMED)) {
        List<OrderItem> orderItems = purchaseOrder.getOrderItems();
        List<OrderItem> orderItemList = orderItems.stream().map(orderItem -> {
            if (orderItem.getOrderStatus().equals(OrderStatus.CONFIRMED)) {
                Product product = orderItem.getProduct();
                if (product != null) {
                    product.setQuantity(product.getQuantity() + orderItem.getQuantity());
                    if (product.getQuantity() > 10) {
                        product.setProductStatus(ProductStatus.INSTOCK);
                    } else if (product.getQuantity() < 10) {
                        product.setProductStatus(ProductStatus.OUTOFSTOCK);
                    }
                    productRepository.save(product);
                }
                orderItem.setOrderStatus(OrderStatus.CANCELLED);
            }
            return orderItem;

        }).collect(Collectors.toList());
        orderItemRepository.saveAll(orderItemList);

    }
    purchaseOrderRepository.deleteById(idPurchaseOrder);
    return idPurchaseOrder;
}

}
