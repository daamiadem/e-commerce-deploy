package com.qa.project.service;
import com.qa.project.model.Client;
import com.qa.project.model.security.Myuser;
import com.qa.project.model.ResetPasswordRequest;
import com.qa.project.repository.ClientRepository;
import com.qa.project.repository.MyuserRepository;
import com.qa.project.repository.ResetPasswordRequestRepository;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Email;
import com.sendgrid.helpers.mail.objects.Personalization;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.util.Objects;
import java.util.Optional;


@Service
@AllArgsConstructor
public class EmailService {

    private final String SENDGRID_API_KEY = "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";
    private final PasswordEncoder passwordEncoder;
    private final ResetPasswordRequestRepository resetPasswordRequestRepository;
    private final TokenGenerator tokenGenerator;
    private final ClientRepository clientRepository;
    private final MyuserRepository myuserRepository;

    public void sendPasswordResetEmail(String toEmail) throws IOException {
        Client client = clientRepository.findClientByEmail(toEmail);
        if (Objects.nonNull(client)){
            String verificationToken = tokenGenerator.generateToken(32);
            Optional<ResetPasswordRequest> myRequest = resetPasswordRequestRepository.findResetPasswordRequestByClientId(client.getId());
            if (myRequest.isPresent()){
                ResetPasswordRequest updatedRequest = myRequest.get();
                updatedRequest.setVerificationToken(verificationToken);
                resetPasswordRequestRepository.save(updatedRequest);
            }else {
                ResetPasswordRequest updatedRequest = ResetPasswordRequest.builder()
                        .client(client)
                        .verificationToken(verificationToken)
                        .build();
                resetPasswordRequestRepository.save(updatedRequest);
            }
            Email from = new Email("hasd.team.contact@gmail.com");
            Email to = new Email(toEmail);
            Mail mail = new Mail();
            mail.setFrom(from);
            mail.setTemplateId("d-2b575783d08f47fba1c9b724c2da349b");
            Personalization personalization = new Personalization();
            personalization.addTo(to);
            String subject = "Mot de passe oublié";
            personalization.setSubject(subject);
            personalization.addDynamicTemplateData("prenom", "mouheb");
            personalization.addDynamicTemplateData("nom", "ben mansoura");
            personalization.addDynamicTemplateData("resetUrl","http://localhost:4200/#/auth/login/reset-password?verificationToken="+ verificationToken);
            mail.addPersonalization(personalization);





            SendGrid sg = new SendGrid(SENDGRID_API_KEY);
            Request request = new Request();
            try {
                request.setMethod(Method.POST);
                request.setEndpoint("mail/send");
                request.setBody(mail.build());
                Response response = sg.api(request);

                System.out.println(response.getStatusCode());
                System.out.println(response.getBody());
                System.out.println(response.getHeaders());
            } catch (IOException ex) {
                throw ex;
            }
        }



    }

    public void resetPassword(String password, String verificationToken) {
        Optional<ResetPasswordRequest> request = resetPasswordRequestRepository.findResetPasswordRequestByVerificationToken(verificationToken);
        if(request.isPresent()){
            String encodedPassword = passwordEncoder.encode(password);
            Optional<Myuser> myuser = myuserRepository.findByUsername(request.get().getClient().getEmail());
            if (myuser.isPresent()){
                Myuser user =  myuser.get();
                user.setPassword(encodedPassword);
                myuserRepository.save(user);
            }
        }
    }
}

