package com.qa.project.service;

import com.qa.project.dto.UserRequest;
import com.qa.project.model.security.Myuser;

import java.util.Optional;

public interface UserService {
    Myuser signUp(UserRequest userRequest);
    Optional<Myuser> findByUsername(String username);

}
