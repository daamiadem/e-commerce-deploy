package com.qa.project.service;

import com.qa.project.dto.ClientDto;
import com.qa.project.dto.UserRequest;
import com.qa.project.model.security.Myuser;
import com.qa.project.model.security.Role;
import com.qa.project.repository.MyuserRepository;
import com.qa.project.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private MyuserRepository myuserRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private ClientService clientService;

    @Override
    public Myuser signUp(UserRequest userRequest) {
        Myuser user = new Myuser();
        user.setUsername(userRequest.getEmail());
        user.setPassword(passwordEncoder.encode(userRequest.getPassword()));

        Set<Role> roles = new HashSet<>();
        Role userRole = roleRepository.findByName(userRequest.getRole());
        Role roleToSave;
        if (Objects.isNull(userRole)) {
            userRole = new Role();
            userRole.setName("ROLE_BUYER");
            roleToSave = roleRepository.save(userRole);
        }else{
            roleToSave = userRole;
        }

        roles.add(userRole);
        user.setRoles(roles);
        myuserRepository.save(user);
        ClientDto clientDto = new ClientDto();
        clientDto.setNom(userRequest.getLastname());
        clientDto.setPrenom(userRequest.getFirstname());
        clientDto.setEmail(userRequest.getEmail());
        clientDto.setDateNaissance(userRequest.getDateNaissance());
        clientDto.setDateCreationCompte(new Date());
        clientDto.setPays("Tunisia");
        clientDto.setVerified(userRequest.getRole().equals("ROLE_BUYER")?true:false);
        clientDto.setPhoneNumber(userRequest.getPhoneNumber());
        clientService.create(clientDto);
        return user;

    }

    @Override
    public Optional<Myuser> findByUsername(String username) {
        return myuserRepository.findByUsername(username);
    }


}

