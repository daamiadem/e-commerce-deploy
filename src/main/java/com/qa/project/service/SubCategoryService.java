package com.qa.project.service;


import com.qa.project.converter.CategoryConverter;
import com.qa.project.converter.SubCategoryConverter;
import com.qa.project.converter.SubCategoryDtoConverter;
import com.qa.project.dto.EnrichedSubCategoryDto;
import com.qa.project.dto.SubCategoryDto;
import com.qa.project.model.Category;
import com.qa.project.model.OrderItem;
import com.qa.project.model.SubCategory;
import com.qa.project.repository.CategoryRepository;
import com.qa.project.repository.OrderItemRepository;
import com.qa.project.repository.SubCategoryRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class SubCategoryService {
private final SubCategoryRepository subCategoryRepository;
private final OrderItemRepository orderItemRepository;
private final CategoryRepository categoryRepository;

public SubCategoryDto addSubCategory(Long id, String name){
    Optional<Category> category = categoryRepository.findById(id);
    if (category.isPresent()){
        SubCategory subCategory = SubCategory.builder().category(category.get()).name(name).build();
        SubCategory subCategorySaved = subCategoryRepository.save(subCategory);
        return SubCategoryConverter.newInstance().convert(subCategorySaved);

    }
    return null;
}
public List<SubCategoryDto> getAllSubCategory(){
    List<SubCategory> subCategories = subCategoryRepository.findAll();
    return subCategories.stream().map(subCategory -> SubCategoryConverter.newInstance().convert(subCategory)).collect(Collectors.toList());
}

public Long deleteSubCategory(Long idSubCategory){
    List<OrderItem> orderItems = orderItemRepository.findByProduct_SubCategory_Id(idSubCategory);
    orderItemRepository.deleteAll(orderItems);
    subCategoryRepository.deleteById(idSubCategory);
    return  idSubCategory;
}

    public EnrichedSubCategoryDto getSubcategoryById(Long subId) {
    Optional<SubCategory> subCategory = subCategoryRepository.findById(subId);
    if (subCategory.isPresent()){
        Category category = categoryRepository.findBySubCategoriesContaining(subCategory.get());
        return EnrichedSubCategoryDto.builder()
                .subCategoryDto(SubCategoryConverter.newInstance().convert(subCategory.get()))
                .categoryDto(CategoryConverter.newInstance().convert(category))
                .build();
    }
    return null;
    }
}
