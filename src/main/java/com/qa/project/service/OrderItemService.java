package com.qa.project.service;

import com.qa.project.converter.OrderItemConverter;
import com.qa.project.dto.OrderItemDto;
import com.qa.project.dto.OrderStatus;
import com.qa.project.model.Client;
import com.qa.project.model.OrderItem;
import com.qa.project.repository.ClientRepository;
import com.qa.project.repository.OrderItemRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Slf4j
public class OrderItemService {
    private final OrderItemRepository orderItemRepository;
    private final RoleService roleService;
    private final ClientRepository clientRepository;

    public List<OrderItemDto> getOrderItemsBySellerAndStatusConfirmed() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object principal = authentication.getPrincipal();
        if (principal instanceof UserDetails) {
            String email = ((UserDetails) principal).getUsername();
            String role = roleService.getRoleByUsername(email).getName();
            if (role.equals("ROLE_SELLER")) {
                Client seller = clientRepository.findClientByEmail(email);
                List<OrderItem> orderItems = orderItemRepository.findAll();
                if (!orderItems.isEmpty()){
                    List<OrderItem> orderItemList = orderItems.stream().filter(orderItem -> orderItem.getProduct().getSeller().equals(seller) && orderItem.getOrderStatus().equals(OrderStatus.CONFIRMED)).collect(Collectors.toList());
                    return orderItemList.stream().map(orderItem -> OrderItemConverter.newInstance().convert(orderItem)).collect(Collectors.toList());
                }else {
                    return null;
                }
            }
        }
        return null;
    }

    public OrderItemDto terminateOrderItem(Long idOrderItem){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object principal = authentication.getPrincipal();
        if (principal instanceof UserDetails) {
            String email = ((UserDetails) principal).getUsername();
            String role = roleService.getRoleByUsername(email).getName();
            if (role.equals("ROLE_SELLER")) {
        OrderItem orderItem = orderItemRepository.findById(idOrderItem).orElse(null);
        orderItem.setOrderStatus(OrderStatus.DELIVERED);
        OrderItem savedOrderItem = orderItemRepository.save(orderItem);
        return OrderItemConverter.newInstance().convert(savedOrderItem);
    }}
    return null ;
    }

    public List<OrderItemDto> getLast6OrderItems(){
        List<OrderItem> allOrderItems = orderItemRepository.findAll();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object principal = authentication.getPrincipal();
        if (principal instanceof UserDetails) {
            String email = ((UserDetails) principal).getUsername();
            Client seller = clientRepository.findClientByEmail(email);
            List<OrderItem> orderItems = allOrderItems.stream().filter(orderItem -> orderItem.getProduct().getSeller().equals(seller)).collect(Collectors.toList());
            orderItems.sort(Comparator.comparing(orderItem -> orderItem.getPurchaseOrder().getOrderDate()));
        List<OrderItem> orderItemList = orderItems.stream().limit(6).collect(Collectors.toList());
        return orderItemList.stream().map(orderItem -> OrderItemConverter.newInstance().convert(orderItem)).collect(Collectors.toList());
    }
    return null;
    }

}