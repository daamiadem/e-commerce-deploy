package com.qa.project.service;

import com.qa.project.controller.SubCategoryController;
import com.qa.project.converter.*;
import com.qa.project.dto.*;
import com.qa.project.model.*;
import com.qa.project.model.security.Myuser;
import com.qa.project.repository.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.internal.Pair;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Slf4j
public class ProductService {
private ProductRepository productRepository;
private final PurchaseOrderRepository purchaseOrderRepository;
private final SubCategoryRepository subCategoryRepository;
private final ClientRepository clientRepository;
private final OrderItemRepository orderItemRepository;
private final RoleService roleService;
private final CategoryService categoryService;


public ProductDto addProduct(ProductDto productDto , Long id_subcategory){
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    Object principal = authentication.getPrincipal();
    if (principal instanceof UserDetails) {
        String email = ((UserDetails) principal).getUsername();
        String role = roleService.getRoleByUsername(email).getName();
        if (role.equals("ROLE_SELLER")){
            Client client = clientRepository.findClientByEmail(email);
            SubCategory subCategory = subCategoryRepository.findById(id_subcategory).orElse(null);
            Product product = ProductDtoConverter.newInstance().convert(productDto);
            product.setSeller(client);
            product.setSubCategory(subCategory);
            if (product.getQuantity()>10){
                product.setProductStatus(ProductStatus.INSTOCK);
            }
            else {
                product.setProductStatus(ProductStatus.LOWSTOCK);
            }
    Product product1 = productRepository.save(product);
    return ProductConverter.newInstance().convert(product1);
    }}
    return null;
}

public ProductDto updateProduct(ProductDto productDto, Long idProduct){
    Product product = productRepository.findById(idProduct).orElse(null);
    product.setProductStatus(productDto.getProductStatus());
    product.setName(productDto.getName());
    product.setPrice(productDto.getPrice());
    product.setDescription(productDto.getDescription());
    product.setQuantity(productDto.getQuantity());
    product.setSubCategory(SubCategoryDtoConverter.newInstance().convert(productDto.getSubCategoryDto()));
    Product product1 = productRepository.save(product);
    return ProductConverter.newInstance().convert(product1);
}
//public List<EnrichedProduct> getAllProducts(){
//    List<Product> products= productRepository.findAll();
//    if (Objects.isNull(products) || products.size() == 0){
//        return new ArrayList<EnrichedProduct>();
//    }
//    List<EnrichedProduct> enrichedProducts = products.stream().map((product) -> {
//        List<OrderItem> orderItems = orderItemRepository.findByProductId(product.getId());
//        List<EnrichedOrder> enrichedOrders = orderItems.stream().map(orderItem -> {
//          return EnrichedOrder.builder()
//                  .id(orderItem.getPurchaseOrder().getId())
//                  .customer(ClientConverter.newInstance().convert(orderItem.getPurchaseOrder().getClient()))
//                  .date(orderItem.getPurchaseOrder().getOrderDate())
//                  .amount(orderItem.getUnitPrice().multiply(BigDecimal.valueOf(orderItem.getQuantity())))
//                  .build();
//        }).collect(Collectors.toList());
//        return EnrichedProduct.builder()
//            .productDto(ProductConverter.newInstance().convert(product))
//            .categoryDto(CategoryConverter.newInstance().convert(product.getSubCategory().getCategory()))
//            .reviewDtos(Objects.nonNull(product.getReviews())?product.getReviews().stream().map(review -> ReviewConverter.newInstance().convert(review)).collect(Collectors.toList()):null)
//            .orders(enrichedOrders)
//            .build();
//        }
//    ).collect(Collectors.toList());
//    return enrichedProducts;
//}

    public List<EnrichedProduct> getAllProducts(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object principal = authentication.getPrincipal();
        if (principal instanceof UserDetails) {
            String email = ((UserDetails) principal).getUsername();
            String role = roleService.getRoleByUsername(email).getName();
            if (role.equals("ROLE_SELLER")){
                Client seller = clientRepository.findClientByEmail(email);
                List<Product> products = productRepository.findBySeller(seller);
                if (Objects.isNull(products) || products.size() == 0) {
                    return new ArrayList<EnrichedProduct>();
                }
                List<EnrichedProduct> enrichedProducts = products.stream().map((product) -> {
                            List<OrderItem> orderItems = orderItemRepository.findAllByProductIdAndOrderStatus(product.getId(),OrderStatus.CONFIRMED);
                            List<EnrichedOrder> enrichedOrders = orderItems.stream().map(orderItem -> {
                                return EnrichedOrder.builder()
                                        .id(orderItem.getPurchaseOrder().getId())
                                        .customer(ClientConverter.newInstance().convert(orderItem.getPurchaseOrder().getClient()))
                                        .date(orderItem.getPurchaseOrder().getOrderDate())
                                        .amount(orderItem.getUnitPrice().multiply(BigDecimal.valueOf(orderItem.getQuantity())))
                                        .status(orderItem.getOrderStatus())
                                        .build();
                            }).collect(Collectors.toList());
                            return EnrichedProduct.builder()
                                    .productDto(ProductConverter.newInstance().convert(product))
                                    .categoryDto(CategoryConverter.newInstance().convert(product.getSubCategory().getCategory()))
                                    .reviewDtos(Objects.nonNull(product.getReviews()) ? product.getReviews().stream().map(review -> ReviewConverter.newInstance().convert(review)).collect(Collectors.toList()) : null)
                                    .orders(enrichedOrders)
                                    .build();
                        }
                ).collect(Collectors.toList());
                return enrichedProducts;
            } else if (role.equals("ROLE_ADMIN")) {
                List<Product> products= productRepository.findAll();
                if (Objects.isNull(products) || products.size() == 0){
                    return new ArrayList<EnrichedProduct>();
                }
                List<EnrichedProduct> enrichedProducts = products.stream().map((product) -> {
                            List<OrderItem> orderItems = orderItemRepository.findByProductId(product.getId());
                            List<EnrichedOrder> enrichedOrders = orderItems.stream().map(orderItem -> {
                                return EnrichedOrder.builder()
                                        .id(orderItem.getPurchaseOrder().getId())
                                        .customer(ClientConverter.newInstance().convert(orderItem.getPurchaseOrder().getClient()))
                                        .date(orderItem.getPurchaseOrder().getOrderDate())
                                        .amount(orderItem.getUnitPrice().multiply(BigDecimal.valueOf(orderItem.getQuantity())))
                                        .build();
                            }).collect(Collectors.toList());
                            return EnrichedProduct.builder()
                                    .productDto(ProductConverter.newInstance().convert(product))
                                    .categoryDto(CategoryConverter.newInstance().convert(product.getSubCategory().getCategory()))
                                    .reviewDtos(Objects.nonNull(product.getReviews())?product.getReviews().stream().map(review -> ReviewConverter.newInstance().convert(review)).collect(Collectors.toList()):null)
                                    .orders(enrichedOrders)
                                    .build();
                        }
                ).collect(Collectors.toList());
                return enrichedProducts;
            }

        }
        return null;
    }

public Long deleteProduct(Long idProduct){
    productRepository.deleteById(idProduct);
    return idProduct;
}
public ProductDto UpdateProductStatusToDeleted(Long idProduct){
    Product product = productRepository.findById(idProduct).orElse(null);
    product.setProductStatus(ProductStatus.DELETED);
    Product saved = productRepository.save(product);
    return ProductConverter.newInstance().convert(saved);
}
public List<ProductDto> getProductBySeller(){
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    String email = new String();
    if (authentication != null && authentication.isAuthenticated()) {
        email = authentication.getName();
    }
    Client client = clientRepository.findClientByEmail(email);
    List<Product> ProductsBySeller = productRepository.findBySeller(client);
    return ProductsBySeller.stream().map(product -> ProductConverter.newInstance().convert(product)).collect(Collectors.toList());
}



    public List<ProductDto> getBestSelledProducts() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object principal = authentication.getPrincipal();
        if (principal instanceof UserDetails) {
            String email = ((UserDetails) principal).getUsername();
            Client seller = clientRepository.findClientByEmail(email);
            String role = roleService.getRoleByUsername(email).getName();
            if (role.equals("ROLE_ADMIN")) {
            List<PurchaseOrder> purchaseOrders = purchaseOrderRepository.findAll();
        List<Product> products = productRepository.findAll();
        List<Pair<Product, Integer>> productsQuantities = products.stream().map(product -> Pair.of(product, 0)).collect(Collectors.toList());

        for (PurchaseOrder purchaseOrder : purchaseOrders) {
            for (OrderItem orderItem : purchaseOrder.getOrderItems()) {
                productsQuantities = updatePairValue(productsQuantities, orderItem.getProduct(), orderItem.getQuantity());
            }
        }
        List<ProductDto> topSoldProducts = productsQuantities.stream()
                .sorted((p1, p2) -> Long.compare(p2.getRight(), p1.getRight()))
                .limit(9)
                .map(pair -> ProductConverter.newInstance().convert(pair.getLeft()))
                .collect(Collectors.toList());
        return topSoldProducts;}
            else if (role.equals("ROLE_SELLER")) {
                List<PurchaseOrder> purchaseOrders = purchaseOrderRepository.findAll();
                List<Product> Allproducts = productRepository.findAll();
                List<Product> products = Allproducts.stream().filter(product -> product.getSeller().equals(seller)).collect(Collectors.toList());
                List<Pair<Product, Integer>> productsQuantities = products.stream().map(product -> Pair.of(product, 0)).collect(Collectors.toList());

                for (PurchaseOrder purchaseOrder : purchaseOrders) {
                    for (OrderItem orderItem : purchaseOrder.getOrderItems()) {
                        productsQuantities = updatePairValue(productsQuantities, orderItem.getProduct(), orderItem.getQuantity());
                    }
                }
                List<ProductDto> topSoldProducts = productsQuantities.stream()
                        .sorted((p1, p2) -> Long.compare(p2.getRight(), p1.getRight()))
                        .limit(9)
                        .map(pair -> ProductConverter.newInstance().convert(pair.getLeft()))
                        .collect(Collectors.toList());
                return topSoldProducts;
            }
            }
        return null ;
}

    public List<Pair<Product, Integer>> updatePairValue(List<Pair<Product, Integer>> pairs, Product product, Integer newValue) {
        for (int i = 0; i < pairs.size(); i++) {
            Pair<Product, Integer> pair = pairs.get(i);
            if (pair.getLeft().equals(product)) {
                Pair<Product, Integer> updatedPair = Pair.of(pair.getLeft(), pair.getRight() + newValue);
                pairs.set(i, updatedPair);
                return pairs;
            }
        }
        return pairs;
    }

    public List<EnrichedBuyerProduct> getProductsByBuyer(Long codeId) {
        List<Product> products;
        if (Objects.isNull(codeId)){
            products= productRepository.findAll();
            if (Objects.isNull(products) || products.size() == 0){
                return new ArrayList<EnrichedBuyerProduct>();
            }
        } else {
            products= productRepository.findAllBySubCategory_id(codeId);
            if (Objects.isNull(products) || products.size() == 0){
                return new ArrayList<EnrichedBuyerProduct>();
            }
        }
            List<EnrichedBuyerProduct> enrichedProducts = products.stream().filter(product -> !product.getProductStatus().equals(ProductStatus.DELETED)).map((product) -> {
                        return EnrichedBuyerProduct.builder()
                                .productDto(ProductConverter.newInstance().convert(product))
                                .categoryDto(CategoryConverter.newInstance().convert(product.getSubCategory().getCategory()))
                                .reviewDtos(Objects.nonNull(product.getReviews())?product.getReviews().stream().map(review -> ReviewConverter.newInstance().convert(review)).collect(Collectors.toList()):null)
                                .build();
                    }
            ).collect(Collectors.toList());
            return enrichedProducts;
        }
}